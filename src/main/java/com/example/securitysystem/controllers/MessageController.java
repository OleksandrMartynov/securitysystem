package com.example.securitysystem.controllers;

import com.example.securitysystem.dtos.message.MessageDTO;
import com.example.securitysystem.entities.User;
import com.example.securitysystem.services.messages.MessageService;
import com.example.securitysystem.services.users.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;

@RestController
@RequestMapping("messages")
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final UserService userService;

    @GetMapping()
    public ResponseEntity<List<MessageDTO>> get() {
        String username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User user = userService.findUserByUsername(username);
        var messages = messageService.getMessagesByUserId(user.getId());
        return ResponseEntity.ok(messages);
    }

    @GetMapping("floor-messages/{floorId}")
    public ResponseEntity<List<MessageDTO>> getFloorMessages(@PathVariable("floorId") Long floorId) {
        var messages = messageService.getFloorMessages(floorId);
        return ResponseEntity.ok(messages);
    }

    @PatchMapping("/{messageId}/mark-as-read")
    public ResponseEntity<Void> markMessageAsRead(@PathVariable("messageId") Long messageId) {
        messageService.markMessageAsReadById(messageId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMessage(@PathVariable("id") Long id){
        messageService.deleteMessage(id);
        return ResponseEntity.noContent().build();
    }

    
}
