package com.example.securitysystem.controllers;

import com.example.securitysystem.dtos.building.CreateBuildingDTO;
import com.example.securitysystem.dtos.building.ResponseBuildingDTO;
import com.example.securitysystem.entities.Building;
import com.example.securitysystem.services.buildings.BuildingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("buildings")
@AllArgsConstructor
public class BuildingController {

    private final BuildingService buildingService;

    @PostMapping
    public ResponseEntity<Building> createBuilding(@RequestBody CreateBuildingDTO body) {
        Building building = buildingService.createBuilding(body);
        return ResponseEntity.status(201).body(building);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseBuildingDTO> getBuilding(@PathVariable("id") Long id) {
        var building = buildingService.getBuilding(id);
        return ResponseEntity.ok(building);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBuilding(@PathVariable("id") Long id) {
        buildingService.deleteBuilding(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/user-buildings")
    public ResponseEntity<List<ResponseBuildingDTO>> getUserBuildings() {
        String username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        var userBuildings = buildingService.getUserBuildings(username);
        return ResponseEntity.ok(userBuildings);
    }
    
}
