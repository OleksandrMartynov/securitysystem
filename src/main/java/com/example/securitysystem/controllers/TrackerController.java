package com.example.securitysystem.controllers;

import com.example.securitysystem.dtos.tracker.TrackerInitializationDTO;
import com.example.securitysystem.dtos.tracker.eventDetectedDTO;
import com.example.securitysystem.dtos.tracker.UpdateTrackerFloorDTO;
import com.example.securitysystem.entities.Tracker;
import com.example.securitysystem.services.floors.FloorService;
import com.example.securitysystem.services.trackers.TrackerService;
import com.example.securitysystem.services.trackers.TrackerEventHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("trackers")
public class TrackerController {
    private final TrackerEventHandler trackerEventHandler;
    private final TrackerService trackerService;
    private final FloorService floorService;

    @PostMapping("detect-event")
    public ResponseEntity<Void> detectEvent(@RequestBody eventDetectedDTO eventDetectedDTO) {
        var accessKey = eventDetectedDTO.getAccessKey();
        if (!trackerService.checkTrackerAccessKey(accessKey)) {
            return ResponseEntity.badRequest().build();
        }
        var trackerMacAddress = eventDetectedDTO.getTrackerMacAddress();
        trackerEventHandler.handleTrackerEventDetected(trackerMacAddress);
        return ResponseEntity.ok().build();
    }

    @PostMapping("initialize")
    public ResponseEntity<Void> initialize(@RequestBody TrackerInitializationDTO body) {
        var accessKey = body.getAccessKey();
        if (!trackerService.checkTrackerAccessKey(accessKey)) {
            return ResponseEntity.badRequest().build();
        }
        trackerService.initializeTracker(body.getTrackerMacAddress(),
                body.getTrackerType());
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/update-floor")
    public ResponseEntity<Void> updateFloor(@PathVariable("id") String id,
                                            @RequestBody UpdateTrackerFloorDTO body) {
        var floor = floorService.findFloorById(body.floorId);
        trackerService.updateFloorByTrackerMacAddress(id, floor);
        var tracker = trackerService.findTrackerByMacAddress(id);
        floorService.addTrackerToFloor(floor, tracker);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/floor-trackers/{floorId}")
    public ResponseEntity<List<Tracker>> getFloorTrackers(@PathVariable("floorId") Long floorId) {
        var trackers = trackerService.findTrackersByFloorId(floorId);
        for (Tracker tracker : trackers) {
            var messages = tracker.getMessages();
            messages.sort((o1, o2) -> o2.getTimestamp().compareTo(o1.getTimestamp()));
        }
        return ResponseEntity.ok(trackers);
    }
}
