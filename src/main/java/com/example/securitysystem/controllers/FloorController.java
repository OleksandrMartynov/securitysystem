package com.example.securitysystem.controllers;

import com.example.securitysystem.dtos.floor.CreateFloorDTO;
import com.example.securitysystem.dtos.floor.ResponseFloorDTO;
import com.example.securitysystem.entities.Floor;
import com.example.securitysystem.services.floors.FloorService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("floors")
@AllArgsConstructor
public class FloorController {

    private final FloorService floorService;

    @PostMapping
    public ResponseEntity<Floor> createFloor(@RequestBody CreateFloorDTO body) {
        Floor floor = floorService.createFloor(body);
        return ResponseEntity.status(201).body(floor);
    }

    @GetMapping("/building-floors/{buildingId}")
    public ResponseEntity<List<ResponseFloorDTO>> getBuildingFloors(@PathVariable("buildingId") Long buildingId) {
        List<ResponseFloorDTO> floors = floorService.getBuildingFloors(buildingId);
        return ResponseEntity.ok(floors);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFloor(@PathVariable("id") Long id) {
        floorService.deleteFloor(id);
        return ResponseEntity.noContent().build();
    }
    
}
