package com.example.securitysystem.controllers;


import com.example.securitysystem.dtos.token.TokenDTO;
import com.example.securitysystem.dtos.user.CreateUserDTO;
import com.example.securitysystem.dtos.user.UpdatePasswordDTO;
import com.example.securitysystem.entities.User;
import com.example.securitysystem.services.users.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("register")
    public ResponseEntity<TokenDTO> registerUser(@RequestBody CreateUserDTO body) {
        TokenDTO tokenDto = userService.register(body);
        return ResponseEntity.ok(tokenDto);
    }

    @PostMapping("login")
    public ResponseEntity<TokenDTO> login(@RequestBody CreateUserDTO body) {
        TokenDTO tokenDto = userService.login(body);
        return ResponseEntity.ok(tokenDto);
    }

    @GetMapping("profile")
    public ResponseEntity<User> getCurrentUserProfile() {
        String username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User user = userService.findUserByUsername(username);
        return ResponseEntity.ok(user);
    }

    @PutMapping()
    public ResponseEntity<User> updatePassword(@RequestBody UpdatePasswordDTO body) {
        String username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User updatedUser = userService.updatePassword(username, body);
        return ResponseEntity.ok(updatedUser);
    }

}
