package com.example.securitysystem.dtos.floor;

import com.example.securitysystem.dtos.message.MessageDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseFloorDTO {
    private Long id;
    private String name;
    private MessageDTO lastMessage;
    private int unreadMessagesCount;
}
