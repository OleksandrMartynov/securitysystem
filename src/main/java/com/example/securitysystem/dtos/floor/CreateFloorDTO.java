package com.example.securitysystem.dtos.floor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CreateFloorDTO {
    private String name;

    private Long buildingId;
}
