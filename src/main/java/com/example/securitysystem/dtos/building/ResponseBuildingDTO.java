package com.example.securitysystem.dtos.building;

import com.example.securitysystem.dtos.message.MessageDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseBuildingDTO {
    private Long id;
    private String name;
    private MessageDTO lastMessage;
    private int unreadMessagesCount;
}
