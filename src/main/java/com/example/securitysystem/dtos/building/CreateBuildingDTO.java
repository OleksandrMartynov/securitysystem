package com.example.securitysystem.dtos.building;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBuildingDTO {
    private String name;
}
