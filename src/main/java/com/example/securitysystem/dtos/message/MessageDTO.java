package com.example.securitysystem.dtos.message;

import com.example.securitysystem.services.trackers.TrackerType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MessageDTO {
    private Long id;
    private String text;
    private LocalDateTime timestamp;
    private String buildingName;
    private TrackerType trackerType;
    private String trackerId;
    private boolean isRead;
}
