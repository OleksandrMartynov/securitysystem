package com.example.securitysystem.dtos.tracker;

import com.example.securitysystem.services.trackers.TrackerType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrackerInitializationDTO {
    private TrackerType trackerType;
    private String trackerMacAddress;
    private String accessKey;
}
