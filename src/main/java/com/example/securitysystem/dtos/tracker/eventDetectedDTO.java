package com.example.securitysystem.dtos.tracker;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class eventDetectedDTO {
    private String trackerMacAddress;
    private String accessKey;
}
