package com.example.securitysystem.repositories;

import com.example.securitysystem.services.trackers.TrackerType;
import com.example.securitysystem.entities.Tracker;
import org.springframework.data.jpa.repository.JpaRepository;
import reactor.util.annotation.NonNull;

import java.util.List;

public interface TrackerRepository extends JpaRepository<Tracker, String> {
    TrackerType findTrackerTypeById(String id);
    List<Tracker> findTrackersByFloorId(long floorId);
    Tracker findTrackerById(String id);
    boolean existsById(@NonNull String id);
}
