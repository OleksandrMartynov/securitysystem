package com.example.securitysystem.repositories;

import com.example.securitysystem.entities.Building;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuildingRepository extends JpaRepository<Building, Long> {
    List<Building> findBuildingsByOwnerId(long ownerId);
}
