package com.example.securitysystem.repositories;

import com.example.securitysystem.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> getMessagesByTrackerIdOrderByTimestampDesc(String trackerId);
    Message findMessageById(long id);
}
