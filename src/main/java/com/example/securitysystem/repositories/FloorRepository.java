package com.example.securitysystem.repositories;

import com.example.securitysystem.entities.Floor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FloorRepository extends JpaRepository<Floor, Long> {
    List<Floor> getFloorsByBuildingId(long buildingId);
    Floor findFloorById(Long id);
}
