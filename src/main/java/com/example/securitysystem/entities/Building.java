package com.example.securitysystem.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@NoArgsConstructor

@Entity
@Table(name = "buildings")
public class Building {
    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Column
    private String name;

    @ManyToOne
    @JoinColumn
    private User owner;

    @JsonManagedReference
    public User getOwner() {
        return owner;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "building")
    private List<Floor> floors;

    @JsonBackReference
    public List<Floor> getFloors() {
        return floors;
    }
}
