package com.example.securitysystem.entities;

import com.example.securitysystem.services.trackers.TrackerType;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;

@Setter
@Entity
@Table(name = "trackers")
public class Tracker {
    @Getter
    @Id
    private String id;

    @Getter
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TrackerType trackerType;

    @ManyToOne
    @JoinColumn
    @OnDelete(action = OnDeleteAction.SET_NULL)
    private Floor floor;

    @JsonManagedReference
    public Floor getFloor() {
        return floor;
    }


    @OneToMany(mappedBy = "tracker",cascade = CascadeType.ALL)
    private List<Message> messages;

    @JsonManagedReference
    public List<Message> getMessages() {
        return messages;
    }
}
