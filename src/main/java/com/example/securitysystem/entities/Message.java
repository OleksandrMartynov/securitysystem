package com.example.securitysystem.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Setter
@Entity
@Table(name = "messages")
public class Message {
    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    private String text;
    @Getter
    private LocalDateTime timestamp;
    @Getter
    private boolean isRead;

    @ManyToOne
    @JoinColumn
    private Tracker tracker;

    @JsonBackReference
    public Tracker getTracker() {
        return tracker;
    }
}
