package com.example.securitysystem.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.LinkedList;
import java.util.List;

@Setter
@ToString

@Entity
@Table(name = "floors")
public class Floor {
    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Column
    private String name;

    @ManyToOne
    @JoinColumn
    private Building building;

    @JsonManagedReference
    public Building getBuilding() {
        return building;
    }

    @OneToMany(mappedBy = "floor",orphanRemoval = false)
    private List<Tracker> trackers;

    @JsonBackReference
    public List<Tracker> getTrackers() {
        return trackers;
    }
}
