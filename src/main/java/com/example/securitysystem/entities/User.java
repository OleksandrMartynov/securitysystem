package com.example.securitysystem.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Setter
@NoArgsConstructor

@Entity
@Table(name = "users")
public class User {
    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Column(nullable = false, unique = true)
    private String username;

    @Getter
    @Column(nullable = false)
    private String password;

    @OneToMany(mappedBy = "owner",cascade = CascadeType.ALL)
    private List<Building> buildings;

    @JsonBackReference
    public List<Building> getBuildings() {
        return buildings;
    }
}
