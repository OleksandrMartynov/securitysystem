package com.example.securitysystem.services.buildings;

import com.example.securitysystem.dtos.building.CreateBuildingDTO;
import com.example.securitysystem.dtos.building.ResponseBuildingDTO;
import com.example.securitysystem.entities.Building;

import java.util.List;

public interface BuildingService {
    Building createBuilding(CreateBuildingDTO dto);

    ResponseBuildingDTO getBuilding(Long id);

    List<ResponseBuildingDTO> getUserBuildings(String username);

    Building findBuildingById(Long id);

    void deleteBuilding(Long id);
    List<Building> findBuildingsByOwnerId(long ownerId);
}
