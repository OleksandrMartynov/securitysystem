package com.example.securitysystem.services.buildings;

import com.example.securitysystem.dtos.building.CreateBuildingDTO;
import com.example.securitysystem.dtos.building.ResponseBuildingDTO;
import com.example.securitysystem.dtos.message.MessageDTO;
import com.example.securitysystem.entities.Building;
import com.example.securitysystem.entities.User;
import com.example.securitysystem.repositories.BuildingRepository;
import com.example.securitysystem.services.messages.MessageService;
import com.example.securitysystem.services.users.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public class BuildingServiceImpl implements BuildingService {
    private final BuildingRepository buildingRepository;
    private final UserService userService;
    private final MessageService messageService;

    public Building createBuilding(CreateBuildingDTO dto) {
        String username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        Building building = new Building();
        building.setName(dto.getName());
        User owner = userService.findUserByUsername(username);
        building.setOwner(owner);
        return buildingRepository.save(building);
    }


    public ResponseBuildingDTO getBuilding(Long id) {
        Building building = buildingRepository.findById(id).orElseThrow();
        return createResponseBuildingDTO(building);
    }

    public Building findBuildingById(Long id) {
        return buildingRepository.findById(id).orElseThrow();
    }

    public void deleteBuilding(Long id) {
        buildingRepository.deleteById(id);
    }

    public List<Building> findBuildingsByOwnerId(long ownerId) {
        return buildingRepository.findBuildingsByOwnerId(ownerId);
    }

    public List<ResponseBuildingDTO> getUserBuildings(String username) {
        User user = userService.findUserByUsername(username);
        if(user!=null){
            List<Building> buildings = buildingRepository.findBuildingsByOwnerId(user.getId());
            return buildings.stream()
                    .map(this::createResponseBuildingDTO)
                    .toList();
        }
        return null;
    }

    private ResponseBuildingDTO createResponseBuildingDTO(Building building) {
        ResponseBuildingDTO buildingDTO = new ResponseBuildingDTO();
        buildingDTO.setName(building.getName());
        buildingDTO.setId(building.getId());
        var buildingMessages = messageService.getBuildingMessages(building.getId())
                .stream()
                .sorted(Comparator.comparing(MessageDTO::getTimestamp))
                .toList();
        if (!buildingMessages.isEmpty()) {
            buildingDTO.setLastMessage(buildingMessages.get(buildingMessages.size() - 1));
            var unreadMessagesCount = (int) buildingMessages.stream()
                    .filter(message -> !message.isRead())
                    .count();
            buildingDTO.setUnreadMessagesCount(unreadMessagesCount);
        }
        return buildingDTO;
    }

    
}
