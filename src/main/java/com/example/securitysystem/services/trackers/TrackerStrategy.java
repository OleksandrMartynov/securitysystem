package com.example.securitysystem.services.trackers;

public interface TrackerStrategy {
    String getTrackerMessage();
}
