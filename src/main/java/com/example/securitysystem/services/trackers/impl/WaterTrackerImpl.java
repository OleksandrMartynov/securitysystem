package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.services.trackers.WaterTracker;
import org.springframework.stereotype.Service;

@Service
public class WaterTrackerImpl implements WaterTracker {

    @Override
    public String getTrackerMessage() {
        return "Water detected";
    }
}
