package com.example.securitysystem.services.trackers;

import com.example.securitysystem.entities.Floor;
import com.example.securitysystem.entities.Tracker;

import java.util.List;

public interface TrackerService {
    void initializeTracker(String trackerMacAddress, TrackerType trackerType);
    List<Tracker> findTrackersByFloorId(long floorId);
    TrackerType findTrackerTypeByMacAddress(String macAddress);
    Tracker findTrackerByMacAddress(String MacAddress);
    void updateFloorByTrackerMacAddress(String trackerMacAddress, Floor floor);

    boolean checkTrackerAccessKey(String accessKey);
}
