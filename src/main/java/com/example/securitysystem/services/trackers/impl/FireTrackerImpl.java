package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.services.trackers.FireTracker;
import org.springframework.stereotype.Service;

@Service
public class FireTrackerImpl implements FireTracker {
    @Override
    public String getTrackerMessage() {
        return "Fire detected";
    }
}
