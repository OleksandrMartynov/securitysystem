package com.example.securitysystem.services.trackers;

public interface TrackerResolver {
    TrackerStrategy getTrackerStrategy(TrackerType trackerType);
}
