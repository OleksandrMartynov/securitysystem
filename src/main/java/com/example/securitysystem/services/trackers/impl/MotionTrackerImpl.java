package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.services.trackers.MotionTracker;
import org.springframework.stereotype.Service;

@Service
public class MotionTrackerImpl implements MotionTracker {
    @Override
    public String getTrackerMessage() {
        return "Motion detected";
    }
}
