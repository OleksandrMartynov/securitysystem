package com.example.securitysystem.services.trackers;

public enum TrackerType {
    MotionTracker,
    WaterTracker,
    FireTracker
}
