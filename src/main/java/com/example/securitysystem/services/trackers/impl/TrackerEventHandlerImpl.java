package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.services.messages.MessageService;
import com.example.securitysystem.services.trackers.TrackerEventHandler;
import com.example.securitysystem.services.trackers.TrackerResolver;
import com.example.securitysystem.services.trackers.TrackerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TrackerEventHandlerImpl implements TrackerEventHandler {
    private final TrackerService trackerService;
    private final TrackerResolver trackerResolver;
    private final MessageService messageService;
    public void handleTrackerEventDetected(String trackerMacAddress) {
        var tracker = trackerService.findTrackerByMacAddress(trackerMacAddress);
        if(tracker == null) return;
        var trackerType = tracker.getTrackerType();
        var trackerStrategy = trackerResolver.getTrackerStrategy(trackerType);
        messageService.createMessageAndSendToUser(trackerStrategy, tracker);
    }
}
