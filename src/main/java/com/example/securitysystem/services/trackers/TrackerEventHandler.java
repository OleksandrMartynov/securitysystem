package com.example.securitysystem.services.trackers;

public interface TrackerEventHandler {
    void handleTrackerEventDetected(String trackerMacAddress);
}
