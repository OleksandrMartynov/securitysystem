package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.entities.Floor;
import com.example.securitysystem.entities.Tracker;
import com.example.securitysystem.repositories.TrackerRepository;
import com.example.securitysystem.services.trackers.TrackerService;
import com.example.securitysystem.services.trackers.TrackerType;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TrackerServiceImpl implements TrackerService {
    private final TrackerRepository trackerRepository;
    private final ConfigurableEnvironment env;

    @Override
    public void initializeTracker(String trackerMacAddress, TrackerType trackerType) {
        String id = hashMacAddress(trackerMacAddress);
        var trackerExists = trackerRepository.existsById(id);
        if (!trackerExists) {
            var tracker = new Tracker();
            tracker.setId(id);
            tracker.setTrackerType(trackerType);
            trackerRepository.save(tracker);
        }
    }

    public void updateFloorByTrackerMacAddress(String trackerMacAddress, Floor floor) {
        var tracker = findTrackerByMacAddress(trackerMacAddress);
        tracker.setFloor(floor);
        trackerRepository.save(tracker);
    }

    @Override
    public List<Tracker> findTrackersByFloorId(long floorId) {
        return trackerRepository.findTrackersByFloorId(floorId);
    }

    @Override
    public TrackerType findTrackerTypeByMacAddress(String macAddress) {
        String id = hashMacAddress(macAddress);
        return trackerRepository.findTrackerTypeById(id);
    }

    @Override
    public Tracker findTrackerByMacAddress(String macAddress) {
        String id = hashMacAddress(macAddress);
        return trackerRepository.findTrackerById(id);
    }

    private String hashMacAddress(String macAddress) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(macAddress.getBytes());
            byte[] hashBytes = md.digest();
            return Base64.getEncoder().encodeToString(hashBytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return macAddress;
    }

    public boolean checkTrackerAccessKey(String accessKey){
        return Objects.equals(accessKey,env.getProperty("app.accessKey"));
    }
}
