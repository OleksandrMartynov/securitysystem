package com.example.securitysystem.services.trackers.impl;

import com.example.securitysystem.services.trackers.TrackerType;
import com.example.securitysystem.services.trackers.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TrackerResolverImpl implements TrackerResolver {

    private final MotionTracker motionTracker;
    private final WaterTracker waterTracker;
    private final FireTracker fireTracker;

    @Override
    public TrackerStrategy getTrackerStrategy(TrackerType trackerType) {
        return switch (trackerType) {
            case MotionTracker -> motionTracker;
            case WaterTracker -> waterTracker;
            case FireTracker -> fireTracker;
            default -> throw new IllegalArgumentException("Unsupported tracker type: " + trackerType);
        };
    }
}

