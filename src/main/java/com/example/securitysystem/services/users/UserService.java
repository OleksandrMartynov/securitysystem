package com.example.securitysystem.services.users;

import com.example.securitysystem.dtos.token.TokenDTO;
import com.example.securitysystem.dtos.user.CreateUserDTO;
import com.example.securitysystem.dtos.user.UpdatePasswordDTO;
import com.example.securitysystem.entities.User;


public interface UserService {
    TokenDTO register(CreateUserDTO dto);

    TokenDTO login(CreateUserDTO dto);

    User findUserByUsername(String username);

    User updatePassword(String username, UpdatePasswordDTO dto);
    Long getCurrentUserId();
}
