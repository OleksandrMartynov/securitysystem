package com.example.securitysystem.services.users;

import com.example.securitysystem.config.JwtTokenProvider;
import com.example.securitysystem.dtos.user.UpdatePasswordDTO;
import com.example.securitysystem.entities.User;
import com.example.securitysystem.repositories.UserRepository;
import com.example.securitysystem.dtos.token.TokenDTO;
import com.example.securitysystem.dtos.user.CreateUserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtProvider;

    public TokenDTO register(CreateUserDTO dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        userRepository.save(user);
        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setToken(jwtProvider.generateToken(user.getUsername()));
        return tokenDTO;
    }

    public TokenDTO login(CreateUserDTO dto) {
        User user = userRepository.findUserByUsername(dto.getUsername());
        if (user != null && passwordEncoder.matches(dto.getPassword(), user.getPassword())) {
            TokenDTO tokenDTO = new TokenDTO();
            tokenDTO.setToken(jwtProvider.generateToken(user.getUsername()));
            return tokenDTO;
        } else {
            throw new RuntimeException("Invalid username or password");
        }
    }

    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    public User updatePassword(String username, UpdatePasswordDTO dto) {
        User user = userRepository.findUserByUsername(username);
        String hashPassword = passwordEncoder.encode(dto.getPassword());
        user.setPassword(hashPassword);
        userRepository.save(user);
        return user;
    }

    @Override
    public Long getCurrentUserId() {
        var username = (String) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        var user = findUserByUsername(username);
        return user.getId();
    }
}
