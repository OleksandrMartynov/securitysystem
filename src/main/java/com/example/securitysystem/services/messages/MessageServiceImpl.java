package com.example.securitysystem.services.messages;

import com.example.securitysystem.dtos.message.MessageDTO;
import com.example.securitysystem.entities.*;
import com.example.securitysystem.repositories.BuildingRepository;
import com.example.securitysystem.repositories.FloorRepository;
import com.example.securitysystem.repositories.MessageRepository;
import com.example.securitysystem.services.trackers.TrackerService;
import com.example.securitysystem.services.trackers.TrackerStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final BuildingRepository buildingRepository;
    private final MessageRepository messageRepository;
    private final FloorRepository floorRepository;
    private final TrackerService trackerService;

    public void createMessageAndSendToUser(TrackerStrategy trackerStrategy, Tracker tracker) {
        var messageText = trackerStrategy.getTrackerMessage();
        var message = new Message();
        message.setRead(false);
        message.setText(messageText);
        message.setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Kiev")));
        message.setTracker(tracker);
        messageRepository.save(message);
        var building = tracker.getFloor().getBuilding();
        var owner = building.getOwner();
        var messageDTO = createMessageDTO(building, tracker, message);
        simpMessagingTemplate.convertAndSend("/topic/" + owner.getUsername() + "/live-messages", messageDTO);
    }

    @Override
    public List<MessageDTO> getMessagesByUserId(long userId) {
        var messages = buildingRepository.findBuildingsByOwnerId(userId).stream()
                .flatMap(building -> floorRepository.getFloorsByBuildingId(building.getId()).stream()
                        .flatMap(floor -> trackerService.findTrackersByFloorId(floor.getId()).stream()
                                .flatMap(tracker -> messageRepository.getMessagesByTrackerIdOrderByTimestampDesc(tracker.getId()).stream()
                                        .map(message -> createMessageDTO(building, tracker, message)))))
                .toList();
        var messagesList = new ArrayList<>(messages);
        messagesList.sort((o1, o2) -> o2.getTimestamp().compareTo(o1.getTimestamp()));
        return messagesList;
    }

    public List<MessageDTO> getFloorMessages(Long floorId) {
        Floor floor = floorRepository.findFloorById(floorId);
        return trackerService.findTrackersByFloorId(floorId).stream()
                .flatMap(tracker -> messageRepository.getMessagesByTrackerIdOrderByTimestampDesc(tracker.getId())
                        .stream()
                        .map(message -> createMessageDTO(floor.getBuilding(), tracker, message)))
                .collect(Collectors.toList());
    }

    public void markMessageAsReadById(Long messageId) {
        var message = messageRepository.findMessageById(messageId);
        message.setRead(true);
        messageRepository.save(message);
    }

    private MessageDTO createMessageDTO(Building building, Tracker tracker, Message message) {
        var messageDTO = new MessageDTO();
        messageDTO.setId(message.getId());
        messageDTO.setText(message.getText());
        messageDTO.setTimestamp(message.getTimestamp());
        messageDTO.setBuildingName(building.getName());
        messageDTO.setTrackerType(tracker.getTrackerType());
        messageDTO.setTrackerId(tracker.getId());
        messageDTO.setRead(message.isRead());
        return messageDTO;
    }

    @Override
    public List<MessageDTO> getBuildingMessages(Long buildingId) {
        return floorRepository.getFloorsByBuildingId(buildingId).stream()
                .flatMap(floor -> trackerService.findTrackersByFloorId(floor.getId()).stream()
                        .flatMap(tracker -> messageRepository.getMessagesByTrackerIdOrderByTimestampDesc(tracker.getId()).stream()
                                .map(message -> createMessageDTO(floor.getBuilding(), tracker, message))))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteMessage(Long id) {
        messageRepository.deleteById(id);
    }
}

