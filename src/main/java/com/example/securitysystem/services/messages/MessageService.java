package com.example.securitysystem.services.messages;

import com.example.securitysystem.dtos.message.MessageDTO;
import com.example.securitysystem.entities.Tracker;
import com.example.securitysystem.services.trackers.TrackerStrategy;

import java.util.List;

public interface MessageService {
    void createMessageAndSendToUser(TrackerStrategy trackerStrategy, Tracker tracker);
    List<MessageDTO> getMessagesByUserId(long userId);
    List<MessageDTO> getFloorMessages(Long floorId);
    List<MessageDTO> getBuildingMessages(Long buildingId);
    void markMessageAsReadById(Long messageId);
    void deleteMessage(Long id);
}
