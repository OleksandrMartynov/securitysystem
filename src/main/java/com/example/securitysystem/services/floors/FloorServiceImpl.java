package com.example.securitysystem.services.floors;

import com.example.securitysystem.dtos.floor.CreateFloorDTO;
import com.example.securitysystem.dtos.floor.ResponseFloorDTO;
import com.example.securitysystem.dtos.message.MessageDTO;
import com.example.securitysystem.entities.Building;
import com.example.securitysystem.entities.Floor;
import com.example.securitysystem.entities.Tracker;
import com.example.securitysystem.repositories.FloorRepository;
import com.example.securitysystem.services.buildings.BuildingService;
import com.example.securitysystem.services.messages.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;


@Service
@RequiredArgsConstructor
public class FloorServiceImpl implements FloorService {

    private final FloorRepository floorRepository;
    private final BuildingService buildingService;
    private final MessageService messageService;

    public Floor createFloor(CreateFloorDTO dto) {
        Floor floor = new Floor();
        floor.setName(dto.getName());
        Building building = buildingService.findBuildingById(dto.getBuildingId());
        floor.setBuilding(building);
        return floorRepository.save(floor);
    }

    @Override
    public List<Floor> getFloorsByBuildingId(long buildingId) {
        return floorRepository.getFloorsByBuildingId(buildingId);
    }

    public List<ResponseFloorDTO> getBuildingFloors(Long buildingId) {
        List<Floor> floors = getFloorsByBuildingId(buildingId);
        return floors.stream()
                .map(this::createResponseFloorDTO)
                .toList();
    }

    private ResponseFloorDTO createResponseFloorDTO(Floor floor) {
        ResponseFloorDTO floorDTO = new ResponseFloorDTO();
        floorDTO.setId(floor.getId());
        floorDTO.setName(floor.getName());
        var floorMessages = messageService.getFloorMessages(floor.getId())
                .stream()
                .sorted(Comparator.comparing(MessageDTO::getTimestamp))
                .toList();
        if (!floorMessages.isEmpty()) {
            floorDTO.setLastMessage(floorMessages.get(floorMessages.size() - 1));
            var unreadMessagesCount = (int) floorMessages.stream()
                    .filter(message -> !message.isRead())
                    .count();
            floorDTO.setUnreadMessagesCount(unreadMessagesCount);
        }
        return floorDTO;
    }

    public void deleteFloor(Long id) {
        floorRepository.deleteById(id);
    }

    @Override
    public Floor findFloorById(Long id) {
        return floorRepository.findFloorById(id);
    }

    @Override
    public void addTrackerToFloor(Floor floor, Tracker tracker) {
        var trackers = floor.getTrackers();
        trackers.add(tracker);
        floorRepository.save(floor);
    }
}
