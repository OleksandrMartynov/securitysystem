package com.example.securitysystem.services.floors;


import com.example.securitysystem.dtos.floor.CreateFloorDTO;
import com.example.securitysystem.dtos.floor.ResponseFloorDTO;
import com.example.securitysystem.entities.Floor;
import com.example.securitysystem.entities.Tracker;

import java.util.List;

public interface FloorService {
    Floor createFloor(CreateFloorDTO dto);
    List<Floor> getFloorsByBuildingId(long buildingId);

    List<ResponseFloorDTO> getBuildingFloors(Long buildingId);

    void deleteFloor(Long id);
    Floor findFloorById(Long id);

    void addTrackerToFloor(Floor floor, Tracker tracker);
}
