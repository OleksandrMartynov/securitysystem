#include <ESP8266WiFi.h>
#include <arduino.h>
#include <stdio.h>
#include <string.h>

#define sensorPower D7
#define sensorPin A0

int wifi_connected_at = 0;

WiFiServer server(80);

char hex_to_num(char hex)
{
    if(hex >= '0' && hex <= '9')
    {
        return hex-'0';
    }

    if(hex >= 'A' && hex <= 'F')
    {
        return hex-'A'+10;
    }

    return 0;
}

char* get_postfix(const char* mac, char* buffer)
{
    char bytes[6];

    bytes[0] = hex_to_num(mac[0])<<4 | hex_to_num(mac[1]);
    bytes[1] = hex_to_num(mac[3])<<4 | hex_to_num(mac[4]);
    bytes[2] = hex_to_num(mac[6])<<4 | hex_to_num(mac[7]);
    bytes[3] = hex_to_num(mac[9])<<4 | hex_to_num(mac[10]);
    bytes[4] = hex_to_num(mac[12])<<4 | hex_to_num(mac[13]);
    bytes[5] = hex_to_num(mac[15])<<4 | hex_to_num(mac[16]);

    char v[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    srand(bytes[0]<<24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3]);
    for(int i = 0; i < 4; ++i) {
        buffer[i] = v[rand()%16];
    }

    srand(bytes[2]<<24 | bytes[3] << 16 | bytes[4] << 8 | bytes[5]);
    for(int i = 4; i < 8; ++i) {
        buffer[i] = v[rand()%16];
    }

    buffer[8] = '\0';

    return buffer;
}

int post_request(const char* host, const int port, const char* endpoint, char* data, char* result)
{
    WiFiClient client;

    if(!client.connect(host, port))
    {
        return -1; // Connection failed
    }

    char request[512];
    sprintf(request,
            "POST %s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "Connection: close\r\n"
            "Accept: */*\r\n"
            "Content-Type: application/json\r\n"
            "Content-Length: %d\r\n"
            "\r\n"
            "%s\r\n",
            endpoint, host, strlen(data), data);


    client.print(request);

    return 0;
}

int create_AP()
{
    IPAddress local_IP(192,168,4,1);
    IPAddress gateway(192,168,4,1);
    IPAddress subnet(255,255,255,0);

    if(!WiFi.softAPConfig(
        local_IP,
        gateway,
        subnet)
    ) return -1;

    char name[32];
    char postfix[10];

    get_postfix(WiFi.macAddress().c_str(), postfix);

    sprintf(name, "ArduinoDetector %s", postfix);

    if(!WiFi.softAP(name, "CatsArePrettyNeat")) return -2;

    return 0;
}

void connect_to_wifi(const char* ssid, const char* password)
{
    WiFi.begin(ssid, password);

    wifi_connected_at = millis();
}

#define WIFI_TIMEOUT_TIME 20000
#define CONNECTION_STATUS (WiFi.status() == WL_CONNECTED ? "Connected" : (wifi_connected_at + WIFI_TIMEOUT_TIME < millis() ? "Not connected" : "Connecting"))
#define CURRENT_WIFI (WiFi.status() == WL_CONNECTED ? WiFi.SSID().c_str() : "-")
#define TRACKER_TYPE "WaterTracker"

#define BACKEND_ENDPOINT "5.183.9.104"
#define BACKEND_PORT 9090

#define TRACKER_TIMEOUT 10000

#define SECRET_PHRASE "ArduinoSecretPhrase"

bool init_sent = false;

void setup()
{
    Serial.begin(115200);

    pinMode(sensorPower, OUTPUT);
    digitalWrite(sensorPower, LOW);

    Serial.println();

    if(create_AP() < 0)
    {
        Serial.println("Failed to create a hotspot");
    }

    server.begin();
}

char* prepareHtmlPage(char* buffer)
{
    sprintf(
        buffer,
        "HTTP/1.1 200 OK\r\n"
        "Content-Type: text/html\r\n"
        "Connection: close\r\n"
        "\r\n"
        "<!DOCTYPE html>"
        "<html>"
        "<body>"
        "<p>Status: %s</p>"
        "<p>Wifi: %s</p>"
        "<form id='networkForm'>"
        "<label for='ssid'>SSID:</label>"
        "<input type='text' id='ssid' name='ssid' required><br><br>"
        "<label for='password'>Password:</label>"
        "<input type='password' id='password' name='password' required><br><br>"
        "<button type='button' onclick='connectToNetwork()'>Connect</button>"
        "</form>"
        "<script>"
        "function connectToNetwork() {"
        "const ssid = document.getElementById('ssid').value;"
        "const password = document.getElementById('password').value;"
        "const data = `${ssid}|${password}|`;"
        "fetch('/connect', {"
        "method: 'POST',"
        "headers: {"
        "'Content-Type': 'text/plain'"
        "},"
        "body: data"
        "});"
        "}"
        "</script>"
        "</body>"
        "</html>"
        "\r\n",

        CONNECTION_STATUS, CURRENT_WIFI);

    return buffer;
}

String invalidRequestResponse()
{
    String htmlPage;
    htmlPage.reserve(1024);               // prevent ram fragmentation
    htmlPage = F("HTTP/1.1 400 Bad Request\r\n"
                "Content-Type: text/html\r\n"
                "Connection: close\r\n"  // the connection will be closed after completion of the response
                "\r\n"
                "<!DOCTYPE HTML>"
                "<html>"
                "Bad request"
                "</html>"
                 "\r\n");
    return htmlPage;
}

enum {
    METHOD_POST = 1,
    METHOD_GET = 2,
    METHOD_UNKNOWN = 3,
};

void runHTTPServer()
{
    WiFiClient client = server.accept();

    if(client)
    {
        Serial.println("\n[Client connected]");

        int current_method = METHOD_UNKNOWN;
        int content_length = 0;

        char data_buffer[256] = {0};
        char method[16] = {0};
        char route[64] = {0};
        char content_size[16] = {0};


        while(client.connected())
        {
            if(client.available())
            {
                String line = client.readStringUntil('\r');
                const char* raw_line = line.c_str();

                if(strstr(raw_line, "HTTP/1.1") != NULL)
                {
                    const char* old_slice_ptr = raw_line;
                    const char* new_slice_ptr = strchr(old_slice_ptr, ' ');
                    
                    strncpy(
                        method, old_slice_ptr, min(
                            (size_t)(new_slice_ptr-old_slice_ptr),
                            sizeof(method)
                        )
                    );

                    old_slice_ptr = new_slice_ptr+1;
                    new_slice_ptr = strchr(old_slice_ptr, ' ');

                    strncpy(
                        route, old_slice_ptr, min(
                            (size_t)(new_slice_ptr-old_slice_ptr),
                            sizeof(route)
                        )
                    );

                    if(!strcmp(method, "GET"))
                    {
                        current_method = METHOD_GET;
                    }
                    else if(!strcmp(method, "POST"))
                    {
                        current_method = METHOD_POST;
                    }
                    else
                    {
                        current_method = METHOD_UNKNOWN;
                    }
                }

                if(strstr(raw_line, "Content-Length:") != NULL)
                {
                    const char* old_slice_ptr = raw_line;
                    const char* new_slice_ptr = strchr(old_slice_ptr, ' ');

                    old_slice_ptr = new_slice_ptr+1;
                    new_slice_ptr = strchr(old_slice_ptr, '\n');

                    strncpy(
                        content_size, old_slice_ptr, min(
                            (size_t)(new_slice_ptr-old_slice_ptr),
                            sizeof(content_size)
                        )
                    );

                    content_length = atoi(content_size);
                }

                // Response time
                if (line.length() == 1 && line[0] == '\n')
                {
                    switch(current_method)
                    {
                        case METHOD_GET:
                        {
                            if(!strcmp(route, "/"))
                            {
                                char htmlBuffer[2048];
                                client.println(prepareHtmlPage(htmlBuffer));
                            }
                            else
                            {
                                client.println(invalidRequestResponse());
                            }

                            break;
                        }
                        case METHOD_POST:
                        {
                            if(!strcmp(route, "/connect") && content_length < sizeof(data_buffer))
                            {
                                char htmlBuffer[2048];
                                client.println(prepareHtmlPage(htmlBuffer));
                            }
                            else
                            {
                                client.println(invalidRequestResponse());
                            }

                            // skip the \n character
                            client.readBytes(data_buffer, 1);

                            client.readBytes(data_buffer, content_length);

                            char connect_ssid[32] = {0};
                            char connect_pwd[32] = {0};

                            const char* old_slice_ptr = data_buffer;
                            const char* new_slice_ptr = strchr(old_slice_ptr, '|');
                            
                            strncpy(
                                connect_ssid, old_slice_ptr, min(
                                    (size_t)(new_slice_ptr-old_slice_ptr),
                                    sizeof(connect_ssid)
                                )
                            );

                            old_slice_ptr = new_slice_ptr+1;
                            new_slice_ptr = strchr(old_slice_ptr, '|');

                            strncpy(
                                connect_pwd, old_slice_ptr, min(
                                    (size_t)(new_slice_ptr-old_slice_ptr),
                                    sizeof(connect_pwd)
                                )
                            );

                            Serial.println(connect_ssid);
                            Serial.println(connect_pwd);

                            connect_to_wifi(connect_ssid, connect_pwd);

                            break;
                        }
                        default:
                        {
                            client.println(invalidRequestResponse());
                            break;
                        }
                    }

                    break;
                }
            }
        }

        while (client.available())
        {
            client.read();
        }

        // close the connection:
        client.stop();
        Serial.println("[Client disconnected]");
    }
}

long previous_value = 0;
long last_trigger = -TRACKER_TIMEOUT;
long last_read = 0;

int readSensor() {
	  digitalWrite(sensorPower, HIGH);
	  delay(10);
    int val = analogRead(sensorPin);
  	digitalWrite(sensorPower, LOW);
  	return val;
}

void loop()
{
    runHTTPServer();

    if(WiFi.status() == WL_CONNECTED && !init_sent)
    {
        char data[256];

        sprintf(data, "{\"trackerMacAddress\": \"%s\", \"trackerType\": \"%s\", \"accessKey\": \"%s\"}",
                WiFi.softAPmacAddress().c_str(), TRACKER_TYPE, SECRET_PHRASE);
        Serial.println(data);
        
        if(!post_request(BACKEND_ENDPOINT, BACKEND_PORT, "/trackers/initialize", data, NULL))
        {
            init_sent = true;
        }
    }

    int value = 0;

    if(last_read + 1000 < millis())
    {
        last_read = millis();
        value = readSensor();
    }

    if(WiFi.status() == WL_CONNECTED && value >= 250 && last_trigger + TRACKER_TIMEOUT < millis())
    {
        char data[256];

        sprintf(data, "{\"trackerMacAddress\": \"%s\", \"accessKey\": \"%s\"}", WiFi.softAPmacAddress().c_str(), SECRET_PHRASE);
        Serial.println(data);

        post_request(BACKEND_ENDPOINT, BACKEND_PORT, "/trackers/detect-event", data, NULL);

        last_trigger = millis();
    }

    previous_value = value;
}
