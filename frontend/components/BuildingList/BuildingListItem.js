import { useNavigation } from "@react-navigation/native";
import {
  Image,
  Text,
  StyleSheet,
  Pressable,
  View,
  TouchableOpacity,
} from "react-native";
import { useDispatch } from "react-redux";

import DraggableBox from "../../UI/DragableBox";
import { removeBuildingAction } from "../../store/slices/buildingSlice";
import { red } from "react-native-reanimated/src";

const RenderItem = ({ building, onPress }) => {
  // const date = lastMessage?.timestamp.slice(0, 10);
  // const time = lastMessage?.timestamp.match(/T([^\.]*)/)[1];

  // const currentDate = new Date().toISOString().slice(0, 10);

  return (
    <Pressable onPress={onPress} style={styles.container}>
      <View style={styles["row-container"]}>
        <Text style={styles.title}>{building.name}</Text>
        <Text>
          {(function () {
            if (building.lastMessage) {
              const date = building.lastMessage.timestamp.slice(0, 10);
              const time = building.lastMessage.timestamp.match(/T([^\.]*)/)[1];
              const currentDate = new Date().toISOString().slice(0, 10);
              return currentDate == date ? time : date;
            }
            return "";
          })()}
        </Text>
      </View>
      <View style={styles["row-container"]}>
        <Text>{building.lastMessage ? building.lastMessage.text : ""}</Text>
        {building.unreadMessagesCount != 0 && (
          <View style={styles["messages-counter"]}>
            <Text style={styles["messages-counter-text"]}>
              {building.unreadMessagesCount}
            </Text>
          </View>
        )}
      </View>
    </Pressable>
  );
};

const BuildingListItem = ({ building }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const pressHandler = () => {
    console.log("Pressed");
    navigation.navigate("FloorsScreen", { buildingId: building.id });
  };

  const dragRightHandler = () => {
    dispatch(removeBuildingAction(building.id));
  };

  return (
    <DraggableBox
      dragRight={dragRightHandler}
      onPress={pressHandler}
      component={<RenderItem building={building} onPress={pressHandler} />}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
    margin: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
  },
  "row-container": {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  "messages-counter": {
    alignSelf: "flex-end",
    backgroundColor: "#015CB1",
    paddingHorizontal: 7,
    paddingVertical: 2,
    borderRadius: 30,
  },
  "messages-counter-text": {
    color: "#ffffff",
  },
});

export default BuildingListItem;
