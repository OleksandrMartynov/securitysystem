import { FlatList, View, StyleSheet } from "react-native";

import BuildingListItem from "./BuildingListItem";

const ItemSeparator = () => {
  return <View style={styles.separator} />;
};

const BuildingList = ({ buildings }) => {
  return (
    <FlatList
      data={buildings}
      keyExtractor={(building) => building.id.toString()}
      renderItem={({ item }) => <BuildingListItem building={item} />}
      ItemSeparatorComponent={ItemSeparator}
    />
  );
};

const styles = StyleSheet.create({
  separator: {
    height: 15,
  },
});

export default BuildingList;
