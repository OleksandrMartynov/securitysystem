import { FlatList, StyleSheet, View } from "react-native";

import FloorListItem from "./FloorListItem";

const ItemSeparator = () => {
  return <View style={styles.separator} />;
};

const FloorList = ({ floors }) => {
  console.log(floors);

  return (
    <FlatList
      style={styles.list}
      data={floors}
      keyExtractor={(floor) => floor.id}
      renderItem={({ item }) => <FloorListItem floor={item} />}
      ItemSeparatorComponent={ItemSeparator}
    />
  );
};

const styles = StyleSheet.create({
  separator: {
    height: 15,
  },
});

export default FloorList;
