import { Text, StyleSheet, Pressable, View } from "react-native";
import { useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import DraggableBox from "../../UI/DragableBox";
import { removeFloorAction } from "../../store/slices/floorSlice";

const RenderItem = ({ floor, onPress }) => {
  // const date = lastMessage?.timestamp.slice(0, 10);
  // const time = lastMessage?.timestamp.match(/T([^\.]*)/)[1];

  // const currentDate = new Date().toISOString().slice(0, 10);

  return (
    <Pressable onPress={onPress} style={styles.container}>
      <View style={styles["row-container"]}>
        <Text style={styles.title}>{floor.name}</Text>
        <Text>
          {(function () {
            if (floor.lastMessage) {
              const date = floor.lastMessage.timestamp.slice(0, 10);
              const time = floor.lastMessage.timestamp.match(/T([^\.]*)/)[1];
              const currentDate = new Date().toISOString().slice(0, 10);
              return currentDate == date ? time : date;
            }
            return "";
          })()}
        </Text>
      </View>
      <View style={styles["row-container"]}>
        <Text>{floor.lastMessage ? floor.lastMessage.text : ""}</Text>
        {floor.unreadMessagesCount != 0 && (
          <View style={styles["messages-counter"]}>
            <Text style={styles["messages-counter-text"]}>
              {floor.unreadMessagesCount}
            </Text>
          </View>
        )}
      </View>
    </Pressable>
  );
};

const FloorListItem = ({ floor }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const dragRightHandler = () => {
    dispatch(removeFloorAction(floor.id));
  };

  const pressHandler = () => {
    navigation.navigate("SingleFloorScreen", { floorId: floor.id });
  };

  return (
    <DraggableBox
      dragRight={dragRightHandler}
      onPress={pressHandler}
      component={<RenderItem floor={floor} onPress={pressHandler} />}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
    margin: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
  },
  "row-container": {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  "messages-counter": {
    alignSelf: "flex-end",
    backgroundColor: "#015CB1",
    paddingHorizontal: 7,
    paddingVertical: 2,
    borderRadius: 30,
  },
  "messages-counter-text": {
    color: "#ffffff",
  },
});

export default FloorListItem;
