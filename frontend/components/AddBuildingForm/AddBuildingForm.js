import { useState } from "react";
import { useDispatch } from "react-redux";
import { View, Text, StyleSheet } from "react-native";

import Input from "../../UI/Input";
import Button from "../../UI/Button";
import { addBuildingAction } from "../../store/slices/buildingSlice";

const AddBuildingForm = () => {
  const [name, setName] = useState("");

  const dispatch = useDispatch();

  const addBuildingHandler = () => {
    dispatch(addBuildingAction({ name }));
  };

  return (
    <View style={styles.form}>
      <Text style={styles.title}>Add Building</Text>
      <Text style={styles.label}>Building title</Text>
      <Input value={name} onChange={(e) => setName(e.nativeEvent.text)} />
      <View style={styles.actions}>
        <Button title={"Create"} onPress={addBuildingHandler} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    rowGap: 10,
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
  actions: {
    marginTop: 20,
  },
});

export default AddBuildingForm;
