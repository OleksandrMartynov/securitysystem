import { Pressable, Text, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";

const BuildingSelectionListItem = ({ building }) => {
  const navigation = useNavigation();

  const pressHandler = () => {
    navigation.navigate("AddFloorScreen", { buildingId: building.id });
  };

  return (
    <Pressable style={styles.container} onPress={pressHandler}>
      <Text style={styles.title}>{building.name}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
  },
});

export default BuildingSelectionListItem;
