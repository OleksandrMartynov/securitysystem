import { FlatList } from "react-native";
import BuildingSelectionListItem from "./BuildingSelectionListItem";

const BuildingSelectionList = ({ buildings }) => {
  return (
    <FlatList
      data={buildings}
      keyExtractor={(building) => building.id.toString()}
      renderItem={({ item }) => <BuildingSelectionListItem building={item} />}
    />
  );
};

export default BuildingSelectionList;
