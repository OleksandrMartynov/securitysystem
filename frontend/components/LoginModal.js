import { View, Text, StyleSheet } from "react-native";
import { useState } from "react";
import { useDispatch } from "react-redux";

import Input from "../UI/Input";
import Button from "../UI/Button";
import { loginAction } from "../store/slices/userSlice";

const LoginModal = () => {
  //const [login, setLogin] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  const loginHandler = () => {
    setEmail("");
    setPassword("");
    dispatch(loginAction({ username: email, password: password }));
  };

  return (
    <View style={styles.form}>
      <Text style={styles.title}>Login</Text>
      <Text style={styles.label}>Email address</Text>
      <Input value={email} onChange={(e) => setEmail(e.nativeEvent.text)} />
      <Text style={styles.label}>Password</Text>
      <Input
        value={password}
        onChange={(e) => setPassword(e.nativeEvent.text)}
      />
      <View style={styles.actions}>
        <Button title={"Login"} onPress={loginHandler} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    rowGap: 10,
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
  actions: {
    marginTop: 20,
  },
});

export default LoginModal;
