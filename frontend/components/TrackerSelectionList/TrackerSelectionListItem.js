import { Pressable, Text, StyleSheet } from "react-native";

const TrackerSelectionListItem = ({ wifi, onPress }) => {
  return (
    <Pressable style={styles.container} onPress={() => onPress(wifi)}>
      <Text style={styles.title}>{wifi.SSID}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    elevation: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
  },
});

export default TrackerSelectionListItem;
