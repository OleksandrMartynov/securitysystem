import { FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useDispatch } from "react-redux";

import { updateTrackerFloorAction } from "../../store/slices/trackerSlice";

import TrackerSelectionListItem from "./TrackerSelectionListItem";

const TrackerSelectionList = ({ wifis, floorId }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const trackerPressHandler = (wifi) => {
    dispatch(updateTrackerFloorAction(wifi.BSSID.toUpperCase(), floorId));
    navigation.goBack({ floorId });
  };

  return (
    <FlatList
      data={wifis}
      keyExtractor={(wifi) => wifi.BSSID}
      renderItem={({ item }) => (
        <TrackerSelectionListItem wifi={item} onPress={trackerPressHandler} />
      )}
    />
  );
};

export default TrackerSelectionList;
