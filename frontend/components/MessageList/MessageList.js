import { FlatList, View, StyleSheet } from "react-native";

import MessageListItem from "./MessageListItem";

const SeparatorItem = () => {
  return <View style={styles.separator} />;
};

const MessageList = ({ messages, tracker }) => {
  console.log(messages);

  return (
    <FlatList
      data={messages}
      keyExtractor={(message) => message.id}
      renderItem={({ item }) => (
        <MessageListItem
          message={{ trackerType: tracker?.trackerType, ...item }}
        />
      )}
      ItemSeparatorComponent={SeparatorItem}
    />
  );
};

const styles = StyleSheet.create({
  separator: {
    height: 15,
  },
});

export default MessageList;
