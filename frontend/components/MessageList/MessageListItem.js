import { Text, StyleSheet, Pressable, View } from "react-native";
import { useDispatch } from "react-redux";

import DraggableBox from "../../UI/DragableBox";
import { markAsReadMessageAction } from "../../store/slices/messageSlice";

const RenderItem = ({ message }) => {
  const date = message.timestamp.slice(0, 10);
  const time = message.timestamp.match(/T([^\.]*)./)[1];

  const currentDate = new Date().toISOString().slice(0, 10);

  return (
    <Pressable style={styles.container}>
      <View style={styles["title-container"]}>
        <Text style={styles.title}>{message.text}</Text>
        <Text>{currentDate == date ? time : date}</Text>
      </View>
      <View style={styles["bottom-container"]}>
        <View style={styles["tag-container"]}>
          {message.trackerType && (
            <View style={styles.tag}>
              <Text style={styles["tag-text"]}>{message.trackerType}</Text>
            </View>
          )}
          {message.buildingName && (
            <View style={styles.tag}>
              <Text style={styles["tag-text"]}>{message.buildingName}</Text>
            </View>
          )}
        </View>
        {!message.read && <View style={styles["unread-mark"]} />}
      </View>
    </Pressable>
  );
};

const MessageListItem = ({ message }) => {
  console.log("My message - ", message);

  const dispatch = useDispatch();

  const dragRightHandler = () => {};

  const dragLeftHandler = () => {
    console.log(message.id);
    dispatch(markAsReadMessageAction(message.id));
  };

  return (
    <DraggableBox
      dragRight={dragRightHandler}
      dragLeft={dragLeftHandler}
      component={<RenderItem message={message} />}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    margin: 5,
    elevation: 5,
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
  },
  "title-container": {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  "tag-container": {
    display: "flex",
    flexDirection: "row",
    gap: 10,
  },
  tag: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#015CB1",
    borderRadius: 10,
  },
  "tag-text": {
    color: "#ffffff",
  },
  "bottom-container": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  "unread-mark": {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: "#015CB1",
  },
});

export default MessageListItem;
