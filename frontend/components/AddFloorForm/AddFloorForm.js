import { useState } from "react";
import { useDispatch } from "react-redux";
import { View, Text, StyleSheet } from "react-native";

import Input from "../../UI/Input";
import Button from "../../UI/Button";
import { addFloorAction } from "../../store/slices/floorSlice";

const AddFloorForm = ({ buildingId }) => {
  const [name, setName] = useState("");

  const dispatch = useDispatch();

  const addFloorHandler = () => {
    dispatch(addFloorAction({ buildingId, name }));
  };

  return (
    <View style={styles.form}>
      <Text style={styles.title}>Add Floor</Text>
      <Text style={styles.label}>Floor title</Text>
      <Input value={name} onChange={(e) => setName(e.nativeEvent.text)} />
      <View style={styles.actions}>
        <Button title={"Create"} onPress={addFloorHandler} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    rowGap: 10,
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
  actions: {
    marginTop: 20,
  },
});

export default AddFloorForm;
