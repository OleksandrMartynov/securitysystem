import Carousel from "react-native-reanimated-carousel";
import { Dimensions, Text, View, StyleSheet, Pressable } from "react-native";
import { forwardRef } from "react";
import { useNavigation } from "@react-navigation/native";

const RenderItem = ({ tracker, floorId }) => {
  const navigation = useNavigation();

  const pressHandler = () => {
    navigation.navigate("TrackerSelectionScreen", { floorId });
  };

  return tracker.button ? (
    <Pressable onPress={pressHandler} style={styles.card}>
      <Text style={styles["add-button"]}>+</Text>
    </Pressable>
  ) : (
    <View style={styles.card}>
      <Text>{tracker.id}</Text>
    </View>
  );
};

const TrackersCarousel = forwardRef(
  ({ trackers, setCurrentTracker, floorId }, ref) => {
    const width = Dimensions.get("window").width;
    const trackersWithButton = [...trackers, { button: true }];

    return (
      <Carousel
        ref={ref}
        loop={false}
        width={width}
        height={width / 2}
        autoPlay={false}
        data={trackersWithButton}
        scrollAnimationDuration={500}
        onSnapToItem={(index) => setCurrentTracker(trackers[index])}
        renderItem={({ index }) => (
          <RenderItem tracker={trackersWithButton[index]} floorId={floorId} />
        )}
      />
    );
  }
);

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: "#ffffff",
    elevation: 5,
    margin: 25,
    borderRadius: 20,
    height: "auto",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  "add-button": {
    fontSize: 80,
    color: "#919191",
    fontWeight: "200",
  },
});

export default TrackersCarousel;
