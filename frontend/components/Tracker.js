import { Image, Text, StyleSheet, View } from "react-native";

const Tracker = ({ tracker }) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.dataContainer}>
          <Text style={styles.title}>{tracker.id}</Text>
          <Text style={[styles.type, { backgroundColor: "#47b7ae" }]}>
            {tracker.trackerType}
          </Text>
        </View>
        <View style={styles.shadow}></View>
      </View>
      {/* <Image source={require("../assets/mc"+ 1 +".png")} style={styles.image}/> */}
    </>
  );
};

const styles = StyleSheet.create({
  shadow: {
    flex: 1,
  },
  dataContainer: {
    flexDirection: "column",
    flex: 2,
  },
  container: {
    backgroundColor: "#ffffff",
    borderRadius: 10,
    padding: 20,
    marginHorizontal: 5,
    marginBottom: 20,
    elevation: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    top: 20,
  },
  title: {
    fontSize: 26,
    fontWeight: "700",
  },
  image: {
    width: 105,
    height: 105,
    resizeMode: "stretch",
    position: "absolute",
    right: 20,
  },
  time: {
    alignSelf: "center",
  },
  type: {
    alignSelf: "flex-end",
    paddingHorizontal: 5,
    paddingVertical: 1,
    borderRadius: 20,
    color: "#ffffff",
  },
  message: {},
});

export default Tracker;
