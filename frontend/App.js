import "react-native-gesture-handler";

import "text-encoding-polyfill";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Provider } from "react-redux";
import { View, StyleSheet } from "react-native";
import { useSelector } from "react-redux/es/hooks/useSelector";
import { useEffect } from "react";
import { useNavigation } from "@react-navigation/native";

import RegistrationScreen from "./screens/RegistrationScreen";
import LoginScreen from "./screens/LoginScreen";
import HomeScreen from "./screens/HomeScreen";

import store from "./store/store";
import Navigation from "./navigation/Navigation";

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer theme={{ colors: { background: "#F4F6FF" } }}>
        <Navigation />
      </NavigationContainer>
    </Provider>
  );
}
