import { createNativeStackNavigator } from "@react-navigation/native-stack";

import BuildingsScreen from "../screens/BuildingsScreen";
import FloorsScreen from "../screens/FloorsScreen";
import SingleFloorScreen from "../screens/SingleFloorScreen";
import TrackerSelectionScreen from "../screens/TrackerSelectionScreen";
import BackButton from "../components/BackButton/BackButton";

const Stack = createNativeStackNavigator();

const BuildingsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTitle: "",
        headerShown: true,
        headerLeft: () => <BackButton />,
        headerStyle: {
          backgroundColor: "#F4F6FF",
        },
      }}
    >
      <Stack.Screen name="BuildingsScreen" component={BuildingsScreen} />
      <Stack.Screen name="FloorsScreen" component={FloorsScreen} />
      <Stack.Screen name="SingleFloorScreen" component={SingleFloorScreen} />
      <Stack.Screen
        name="TrackerSelectionScreen"
        component={TrackerSelectionScreen}
      />
    </Stack.Navigator>
  );
};

export default BuildingsStack;
