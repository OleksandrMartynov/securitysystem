import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useEffect } from "react";
import { Ionicons } from "@expo/vector-icons";
import { StyleSheet, Alert } from "react-native";
import { Client } from "@stomp/stompjs";
import { useDispatch, useSelector } from "react-redux";

import BuildingsStack from "./BuildingsStack";
import HomeDrawerStack from "./HomeDrawerStack";
import DrawerButton from "./drawer/DrawerButton";
import ProfileScreen from "../screens/ProfileScreen";
import MessagesScreen from "../screens/MessagesScreen";
import messageSlice from "../store/slices/messageSlice";
import TrackerSelectionScreen from "../screens/TrackerSelectionScreen";
import BackButton from "../components/BackButton/BackButton";
import trackerSlice from "../store/slices/trackerSlice";

const Tab = createBottomTabNavigator();

const BottomTabNavigation = () => {
  const username = useSelector((state) => state.user.username);
  const token = useSelector((state) => state.user.token);
  const dispatch = useDispatch();

  useEffect(() => {
    let messageSubscription = null;

    const client = new Client({
      brokerURL: "ws://5.183.9.104:9090/security-system",
      onConnect: () => {
        messageSubscription = client.subscribe(
          `/topic/${username}/live-messages`,
          (message) => {
            const response = JSON.parse(message.body);
            Alert.alert(response.text, response.text);
            dispatch(messageSlice.actions.addMessage(response));
            dispatch(trackerSlice.actions.addMessage(response));
          }
        );
        //client.publish({ destination: "/topic/test01", body: "First Message" });
        console.log("Connected");
      },
      onStompError: () => {
        console.log("error");
      },
      onWebSocketError: (error) => {
        console.log(error);
      },
      forceBinaryWSFrames: true,
      appendMissingNULLonIncoming: true,
    });

    client.activate();

    return () => {
      if (messageSubscription) messageSubscription.unsubscribe();
      client.deactivate();
    };
  }, []);

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === "HomeDrawerStack") {
            iconName = focused ? "home" : "home-outline";
          } else if (route.name === "BuildingsStack") {
            iconName = focused ? "albums" : "albums-outline";
          } else if (route.name === "Profile") {
            iconName = focused ? "person" : "person-outline";
          } else if (route.name === "Messages") {
            iconName = focused ? "alert" : "alert-outline";
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: "#015CB1",
        tabBarInactiveTintColor: "#919191",
        tabBarStyle: {
          borderTopWidth: 0,
          height: 60,
          backgroundColor: "#ffffff",
          display: "flex",
          justifyContent: "space-around",
          elevation: 5,
        },
        headerTitle: "",
        headerStyle: {
          backgroundColor: "transparent",
        },
      })}
    >
      <Tab.Screen
        name="HomeDrawerStack"
        component={HomeDrawerStack}
        options={{
          headerLeft: () => <DrawerButton />,
        }}
      />
      <Tab.Screen
        options={{ headerShown: false }}
        name="BuildingsStack"
        component={BuildingsStack}
      />
      <Tab.Screen
        options={{
          headerLeft: () => <DrawerButton />,
        }}
        name="Messages"
        component={MessagesScreen}
      />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigation;

const styles = StyleSheet.create({
  bar: {
    position: "absolute",
    bottom: 20,
    left: 20,
    right: 20,
    elevation: 0,
    borderRadius: 20,
    height: -30,
  },
});
