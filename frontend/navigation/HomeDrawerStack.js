import { createNativeStackNavigator } from "@react-navigation/native-stack";

import HomeScreen from "../screens/HomeScreen";
import AddBuildingScreen from "../screens/AddBuildingScreen";
import BuildingSelectionScreen from "../screens/BuildingSelectionScreen";
import AddFloorScreen from "../screens/AddFloorScreen";

const Stack = createNativeStackNavigator();

const HomeDrawerStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="AddBuildingScreen" component={AddBuildingScreen} />
      <Stack.Screen
        name="BuildingSelectionScreen"
        component={BuildingSelectionScreen}
      />
      <Stack.Screen name="AddFloorScreen" component={AddFloorScreen} />
    </Stack.Navigator>
  );
};

export default HomeDrawerStack;
