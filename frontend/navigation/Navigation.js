import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useNavigation } from "@react-navigation/native";

import EventSource from "react-native-sse";
import RNEventSource from "react-native-event-source";

import RegistrationScreen from "../screens/RegistrationScreen";
import LoginScreen from "../screens/LoginScreen";
import BottomTabNavigation from "./BottomTabNavigation";
import ProfileScreen from "../screens/ProfileScreen";

const Stack = createNativeStackNavigator();

const Navigation = () => {
  const token = useSelector((state) => state.user.token);
  const navigation = useNavigation();

  useEffect(() => {
    if (token != null) {
      navigation.navigate("BottomTabNavigation");
    } else {
      navigation.navigate("RegistrationScreen");
    }
  }, [token]);

  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen
          name="RegistrationScreen"
          component={RegistrationScreen}
        />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen
          name="BottomTabNavigation"
          component={BottomTabNavigation}
        />
      </Stack.Navigator>
    </>
  );
};

export default Navigation;
