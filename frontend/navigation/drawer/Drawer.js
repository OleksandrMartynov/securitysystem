import { View, Text, TouchableOpacity, Modal, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useDispatch } from "react-redux";

import { logoutAction } from "../../store/slices/userSlice";

const Drawer = ({ visible, onClose }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  return (
    <Modal transparent={true} visible={visible} animationType="side">
      <View style={styles.drawerContainer}>
        <View style={styles.drawerContent}>
          <TouchableOpacity onPress={onClose}>
            <Text>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onClose();
              navigation.navigate("AddBuildingScreen");
            }}
          >
            <Text>Add Building</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onClose();
              navigation.navigate("BuildingSelectionScreen");
            }}
          >
            <Text>Add Floor</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onClose();
              dispatch(logoutAction());
            }}
          >
            <Text>Log Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  drawerContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "flex-end",
  },
  drawerContent: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",
    padding: 16,
    position: "absolute",
    left: 0,
    width: "60%", // Adjust the width as needed
    height: "100%", // Make it cover the entire height
  },
});

export default Drawer;
