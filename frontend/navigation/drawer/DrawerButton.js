import { useState } from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import Drawer from "./Drawer";

const DrawerButton = () => {
  const [isDrawerVisible, setDrawerVisible] = useState(false);

  const openDrawer = () => {
    setDrawerVisible(true);
  };

  const closeDrawer = () => {
    setDrawerVisible(false);
  };

  return (
    <>
      <TouchableOpacity style={styles.button} onPress={openDrawer}>
        <Ionicons name="menu" size={40} />
      </TouchableOpacity>
      <Drawer visible={isDrawerVisible} onClose={closeDrawer} />
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#ffffff",
    elevation: 10,
    padding: 3,
    borderRadius: 12,
  },
});

export default DrawerButton;
