import AddFloorForm from "../components/AddFloorForm/AddFloorForm";

const AddFloorScreen = ({ route }) => {
  const buildingId = route.params.buildingId;

  return <AddFloorForm buildingId={buildingId} />;
};

export default AddFloorScreen;
