import AddBuildingForm from "../components/AddBuildingForm/AddBuildingForm";

const AddBuildingScreen = () => {
  return <AddBuildingForm />;
};

export default AddBuildingScreen;
