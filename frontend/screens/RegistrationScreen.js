import { StyleSheet, Text, View } from "react-native";

import RegistrationModal from "../components/RegistrationModal";

const RegistrationScreen = ({ navigation }) => {
  const switchScreenPressHandler = () => {
    navigation.navigate("LoginScreen");
  };

  return (
    <>
      <RegistrationModal />
      <View style={styles["switcher-container"]}>
        <Text style={{ fontSize: 16 }}>Already have an account?</Text>
        <Text style={styles.switcher} onPress={switchScreenPressHandler}>
          Login
        </Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "switcher-container": {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
  },
  switcher: {
    color: "#015CB1",
    fontSize: 16,
  },
});

export default RegistrationScreen;
