import { useSelector, useDispatch } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { View, StyleSheet } from "react-native";

import { getTrackersAction } from "../store/slices/trackerSlice";

import TrackersCarousel from "../components/TrackersCarousel/TrackersCarousel";
import MessageList from "../components/MessageList/MessageList";

const SingleFloorScreen = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const trackers = useSelector((store) => store.trackers.trackers);
  const [currentTracker, setCurrentTracker] = useState(trackers[0]);

  const carouselRef = useRef();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getTrackersAction(route.params.floorId));
      const currentIndex = carouselRef.current.getCurrentIndex();
      carouselRef.current.prev(-currentIndex);
      console.log(currentIndex);
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    setCurrentTracker(trackers[0]);
  }, [trackers]);

  return (
    <>
      <TrackersCarousel
        ref={carouselRef}
        trackers={trackers}
        setCurrentTracker={setCurrentTracker}
        floorId={route.params.floorId}
      />
      <View style={styles["screen-container"]}>
        <MessageList
          messages={currentTracker?.messages}
          tracker={currentTracker}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "screen-container": {
    margin: 20,
    marginBottom: 0,
  },
});

export default SingleFloorScreen;
