import { Text, StyleSheet } from "react-native";

const HomeScreen = () => {
  return <Text style={styles.title}>Home</Text>;
};

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
});

export default HomeScreen;
