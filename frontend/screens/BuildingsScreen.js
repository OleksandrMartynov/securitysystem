import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getBuildingsAction } from "../store/slices/buildingSlice";
import BuildingList from "../components/BuildingList/BuildingList";
import { View } from "react-native-reanimated/src/Animated";
import { StyleSheet, Text } from "react-native";

const BuildingsScreen = ({ navigation, route }) => {
  const buildings = useSelector((state) => state.buildings.buildings);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getBuildingsAction());
    });

    return unsubscribe;
  }, []);

  return (
    <>
      <Text style={styles.title}>My Buildings</Text>
      <View style={styles["screen-container"]}>
        <BuildingList buildings={buildings} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "screen-container": {
    margin: 20,
    marginBottom: 0,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
});

export default BuildingsScreen;
