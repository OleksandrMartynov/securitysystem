import { useEffect, useState } from "react";
import { PermissionsAndroid } from "react-native";
import WifiManager from "react-native-wifi-reborn";

import TrackerSelectionList from "../components/TrackerSelectionList/TrackerSelectionList";

const TrackerSelectionScreen = ({ navigation, route }) => {
  const [wifis, setWifis] = useState([]);

  useEffect(() => {
    navigation.addListener("focus", async () => {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location permission is required for WiFi connections",
          message:
            "This app needs location permission as this is required  " +
            "to scan for wifi networks.",
          buttonNegative: "DENY",
          buttonPositive: "ALLOW",
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const wifis = await WifiManager.loadWifiList();
        console.log(wifis);
        setWifis(wifis);
      } else {
        navigation.navigate("SingleFloorScreen", {
          floorId: route.params.floorId,
        });
      }
    });
  }, []);

  return (
    <TrackerSelectionList
      wifis={wifis.filter((wifi) => wifi.SSID.includes("ArduinoDetector"))}
      floorId={route.params.floorId}
    />
  );
};

export default TrackerSelectionScreen;
