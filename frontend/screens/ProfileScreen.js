import {StyleSheet, Text, View} from "react-native";
import Input from "../UI/Input";
import Button from "../UI/Button";
import {useEffect, useState} from "react";

const ProfileScreen = () => {
    const [name, setName] = useState("null");
    const [email, setEmail] = useState("null");
    const [phone, setPhone] = useState("+380-**-***-****");
    const [password, setPassword] = useState("**********");
    return (
        <View style={styles.form}>
            <Text style={styles.label}>Name</Text>
            <Input value={name} onChange={(event) => setName(event.nativeEvent.text)}/>
            <Text style={styles.label}>Email address</Text>
            <Input value={email} onChange={(event) => setEmail(event.nativeEvent.text)}/>
            <Text style={styles.label}>Phone</Text>
            <Input value={phone} onChange={(event) => setPhone(event.nativeEvent.text)}/>
            <Text style={styles.label}>Password</Text>
            <Input value={email} onChange={(event) => setEmail(event.nativeEvent.text)}/>
            <View style={styles.actions}>
                <Button title={"Save"}/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    form: {
        rowGap: 10,
        marginHorizontal: 15,
    },
    label: {
        fontSize: 16,
        fontWeight: "500",
    },
    title: {
        fontSize: 30,
        fontWeight: "bold",
    },
    actions: {
        marginTop: 20,
        alignSelf: "flex-end"
    },
});

export default ProfileScreen