import { StyleSheet, Text, View } from "react-native";

import LoginModal from "../components/LoginModal";

const LoginScreen = ({ navigation }) => {
  const switchScreenPressHandler = () => {
    navigation.navigate("RegistrationScreen");
  };

  return (
    <>
      <LoginModal />
      <View style={styles["switcher-container"]}>
        <Text style={{ fontSize: 16 }}>Don't have an account?</Text>
        <Text style={styles.switcher} onPress={switchScreenPressHandler}>
          Register
        </Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "switcher-container": {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
  },
  switcher: {
    color: "#015CB1",
    fontSize: 16,
  },
});

export default LoginScreen;
