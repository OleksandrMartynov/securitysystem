import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFloorsAction } from "../store/slices/floorSlice";
import FloorList from "../components/FloorList/FloorList";
import { StyleSheet, Text, View } from "react-native";

const FloorsScreen = ({ navigation, route }) => {
  const floors = useSelector((state) => state.floors.floors);
  const dispatch = useDispatch();

  console.log(floors);

  const buildingId = route.params.buildingId;

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getFloorsAction(buildingId));
    });

    return unsubscribe;
  }, []);

  return (
    <>
      <Text style={styles.title}>Floors</Text>
      <View style={styles["screen-container"]}>
        <FloorList floors={floors} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "screen-container": {
    margin: 20,
    marginBottom: 0,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
});

export default FloorsScreen;
