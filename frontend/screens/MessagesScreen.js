import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet, Text } from "react-native";

import { getAllMessagesAction } from "../store/slices/messageSlice";

import MessageList from "../components/MessageList/MessageList";

const MessagesScreen = ({ navigation }) => {
  const messages = useSelector((state) => state.messages.messages);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getAllMessagesAction());
    });

    return unsubscribe;
  }, []);

  return (
    <>
      <Text style={styles.title}>My Messages</Text>
      <View style={styles["screen-container"]}>
        <MessageList messages={messages} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  "screen-container": {
    margin: 20,
    marginBottom: 0,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
});

export default MessagesScreen;
