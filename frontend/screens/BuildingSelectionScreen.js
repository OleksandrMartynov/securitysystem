import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

import BuildingSelectionList from "../components/BuildingSelectionList/BuildingSelectionList";
import { getBuildingsAction } from "../store/slices/buildingSlice";

const BuildingSelectionScreen = ({ navigation }) => {
  const buildings = useSelector((store) => store.buildings.buildings);
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(getBuildingsAction());
    });

    return unsubscribe;
  }, []);

  return <BuildingSelectionList buildings={buildings} />;
};

export default BuildingSelectionScreen;
