import { createSlice } from "@reduxjs/toolkit";

const buildingSlice = createSlice({
  name: "buildings",
  initialState: {
    buildings: [],
  },
  reducers: {
    getBuildings: (state, action) => {
      state.buildings = action.payload;
    },
    addBuilding: (state, action) => {
      state.buildings.push(action.payload);
    },
    removeBuilding: (state, action) => {
      state.buildings = state.buildings.filter(
        (value) => value.id !== action.payload.id
      );
    },
    clearBuildings: (state) => {
      state.buildings = [];
    },
  },
});

export const getBuildingsAction = () => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(
      "http://5.183.9.104:9090/buildings/user-buildings",
      {
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    const responseData = await response.json();
    console.log(responseData);
    dispatch(buildingSlice.actions.getBuildings(responseData));
  };
};

export const addBuildingAction = (building) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch("http://5.183.9.104:9090/buildings", {
      method: "POST",
      body: JSON.stringify(building),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    dispatch(buildingSlice.actions.addBuilding(responseData));
  };
};

export const removeBuildingAction = (id) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(`http://5.183.9.104:9090/buildings/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    dispatch(buildingSlice.actions.removeBuilding({ id }));
  };
};

export default buildingSlice;
