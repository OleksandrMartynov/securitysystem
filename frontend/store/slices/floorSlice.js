import { createSlice } from "@reduxjs/toolkit";

const floorSlice = createSlice({
  name: "floors",
  initialState: {
    floors: [],
  },
  reducers: {
    getFloors: (state, action) => {
      state.floors = action.payload;
    },
    addFloor: (state, action) => {
      state.floors.push(action.payload);
    },
    removeFloor: (state, action) => {
      state.floors = state.floors.filter(
        (value) => value.id !== action.payload.id
      );
    },
  },
});

export const getFloorsAction = (buildingId) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;

    console.log("start fetching");
    const response = await fetch(
      `http://5.183.9.104:9090/floors/building-floors/${buildingId}`,
      {
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    const responseData = await response.json();
    console.log("response data: " + responseData);
    dispatch(floorSlice.actions.getFloors(responseData));
  };
};

export const addFloorAction = (floor) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch("http://5.183.9.104:9090/floors", {
      method: "POST",
      body: JSON.stringify(floor),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    dispatch(floorSlice.actions.addFloor(responseData));
  };
};

export const removeFloorAction = (floorId) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(`http://5.183.9.104:9090/floors/${floorId}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    dispatch(floorSlice.actions.removeFloor({ id: floorId }));
  };
};

export default floorSlice;
