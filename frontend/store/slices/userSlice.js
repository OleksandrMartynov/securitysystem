import { createSlice } from "@reduxjs/toolkit";

import buildingSlice from "./buildingSlice";

const userSlice = createSlice({
  name: "user",
  initialState: {
    token: null,
    username: "",
  },
  reducers: {
    register: (state) => {},
    login: (state, action) => {
      state.token = action.payload.token;
      state.username = action.payload.username;
    },
    logout: (state) => {
      state.token = null;
      state.username = "";
    },
  },
});

export const userActions = userSlice.actions;

export const registerAction = (user) => {
  return async (dispatch) => {
    const response = await fetch("http://5.183.9.104:9090/users/register", {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const responseData = await response.json();
    dispatch(userSlice.actions.register());
  };
};

export const loginAction = (user) => {
  return async (dispatch) => {
    const response = await fetch("http://5.183.9.104:9090/users/login", {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const responseData = await response.json();
      dispatch(
        userSlice.actions.login({ ...responseData, username: user.username })
      );
    } else {
      console.log(await response.text());
    }
  };
};

export const logoutAction = () => {
  return (dispatch) => {
    dispatch(buildingSlice.actions.clearBuildings());
    dispatch(userSlice.actions.logout());
  };
};

export default userSlice;
