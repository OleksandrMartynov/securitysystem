import { createSlice } from "@reduxjs/toolkit";
import trackerSlice from "./trackerSlice";

const messageSlice = createSlice({
  name: "messages",
  initialState: {
    messages: [],
  },
  reducers: {
    getAllMessages: (state, action) => {
      state.messages = action.payload;
    },
    addMessage: (state, action) => {
      const temp = [...state.messages];
      temp.unshift(action.payload);
      state.messages = temp;
    },
    markAsReadMessage: (state, action) => {
      const message = state.messages.filter(
        (value) => value.id == action.payload.messageId
      )[0];
      message.read = true;
    },
  },
});

export const getAllMessagesAction = () => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(`http://5.183.9.104:9090/messages`, {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    dispatch(messageSlice.actions.getAllMessages(responseData));
  };
};

// export const getMessagesByFloorAction = (floorId) => {
//   return async (dispatch, getState) => {
//     const token = getState().user.token;
//     const response = await fetch(
//       `http://192.168.0.102:8080/messages/floor-messages/${floorId}`,
//       {
//         headers: {
//           Authorization: "Bearer " + token,
//         },
//       }
//     );
//     const responseData = await response.json();
//     dispatch(messageSlice.actions.getMessagesByFloor(responseData));
//   };
// };

export const markAsReadMessageAction = (messageId) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(
      `http://5.183.9.104:9090/messages/${messageId}/mark-as-read`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    );
    dispatch(messageSlice.actions.markAsReadMessage({ messageId }));
    dispatch(trackerSlice.actions.markAsReadTrackersMessage({ messageId }));
  };
};

export default messageSlice;
