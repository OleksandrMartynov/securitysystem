import { createSlice } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

const userWorkSlice = createSlice({
  name: "userWork",
  initialState: {
    buildings: [],
  },
  reducers: {
    addBuilding: (state, action) => {
      state.buildings.push(action.payload);
    },
    // can be with JSON.stringify
    removeBuilding: (state, action) => {
      state.buildings = state.buildings.filter((value, index) => {
        return (
          value.id !== action.payload.id &&
          value.name !== action.payload.name &&
          value.user.password !== action.payload.user.password
        );
      });
    },
    addFloor: (state, action) => {
      const building = state.buildings.filter(
        (b) => b.id === action.payload.buildingId
      );
      building.floors.push(action.payload.floor);
    },
    removeFloor: (state, action) => {
      const building = state.buildings.filter(
        (b) => b.id === action.payload.buildingId
      );
      building.floors.splice(building.floors.indexOf(action.payload.floor), 1);
    },
    addTracker: (state, action) => {
      const building = state.buildings.filter(
        (b) => b.id === action.payload.buildingId
      );
      const floor = building.floors.filter(
        (f) => f.id === action.payload.floorId
      );
      floor.trackers.push(action.payload.tracker);
    },
    removeTracker: (state, action) => {
      const building = state.buildings.filter(
        (b) => b.id === action.payload.buildingId
      );
      const floor = building.floors.filter(
        (f) => f.id === action.payload.floorId
      );
      floor.trackers.splice(floor.trackers.indexOf(action.payload.tracker), 1);
    },
  },
});

export default buildingSlice;

export const getBuildingsAction = () => {
  return async () => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/user-buildings", {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    return responseData.message;
  };
};

export const addBuildingAction = (building) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/buildings", {
      method: "POST",
      body: JSON.stringify(building),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    dispatch(userWorkSlice.actions.addBuilding(building));
  };
};

export const removeBuildingAction = (building) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch(
      `http://192.168.0.104:8080/buildings/${building.id}`,
      {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    dispatch(userWorkSlice.actions.removeBuilding(building));
  };
};

export const getFloorsAction = (buildingId) => {
  return async () => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/user-buildings", {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    const building = responseData.message.find((b) => b.id === buildingId);
    return building.floors;
  };
};

export const addFloorAction = (floor) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/floors", {
      method: "POST",
      body: JSON.stringify(floor),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    dispatch(userWorkSlice.actions.addFloor(floor));
  };
};

export const removeFloorAction = (floor) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch(
      `http://192.168.0.104:8080/floors/${floor.id}`,
      {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    dispatch(userWorkSlice.actions.removeFloor(floor));
  };
};

export const getTrackersAction = (buildingId, floorId) => {
  return async () => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/user-buildings", {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    const responseData = await response.json();
    const building = responseData.message.find((b) => b.id === buildingId);
    const floor = building.floors.find((f) => f.id === floorId);
    return floor.trackers;
  };
};

export const addTrackerAction = (tracker) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch("http://192.168.0.104:8080/trackers", {
      method: "POST",
      body: JSON.stringify(tracker),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    dispatch(userWorkSlice.actions.addTracker(tracker));
  };
};

export const removeTrackerAction = (tracker) => {
  return async (dispatch) => {
    const token = useSelector((state) => state.user.token);
    const response = await fetch(
      `http://192.168.0.104:8080/trackers/${tracker.id}`,
      {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    dispatch(userWorkSlice.actions.removeTracker(tracker));
  };
};
