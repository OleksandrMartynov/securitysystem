import { createSlice } from "@reduxjs/toolkit";

const trackerSlice = createSlice({
  name: "trackers",
  initialState: {
    trackers: [],
  },
  reducers: {
    getTrackers: (state, action) => {
      state.trackers = action.payload;
    },
    addMessage: (state, action) => {
      const tracker = state.trackers.filter(
        (tracker) => tracker.id == action.payload.trackerId
      )[0];
      tracker.messages.unshift(action.payload);
    },
    markAsReadTrackersMessage: (state, action) => {
      const tracker = state.trackers.filter(
        (tracker) =>
          tracker.messages.filter((m) => m.id == action.payload.messageId)
            .length > 0
      )[0];
      const message = tracker.messages.filter(
        (m) => m.id == action.payload.messageId
      )[0];
      message.read = true;
    },
  },
});

export const getTrackersAction = (floorId) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(
      `http://5.183.9.104:9090/trackers/floor-trackers/${floorId}`,
      {
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
    const responseData = await response.json();
    dispatch(trackerSlice.actions.getTrackers(responseData));
  };
};

export const updateTrackerFloorAction = (trackerId, floorId) => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    const response = await fetch(
      `http://5.183.9.104:9090/trackers/${trackerId}/update-floor`,
      {
        method: "PATCH",
        body: JSON.stringify({ id: trackerId, floorId: floorId }),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    );
  };
};

export const liveMessages = () => {
  return async (dispatch, getState) => {
    const token = getState().user.token;
    console.log("heere");
    const response = await fetch(
      "http://5.183.9.104:9090/messages/live-messages",
      {
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          Connection: "keep-alive",
          "Content-Type": "text/event-stream",
          "Cache-Control": "no-cache",
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    console.log("somewhere");
    const responseData = await response.json();
    console.log(responseData);
  };
};

export default trackerSlice;
