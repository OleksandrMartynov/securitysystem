import { configureStore } from "@reduxjs/toolkit";

import userSlice from "./slices/userSlice";
import buildingSlice from "./slices/buildingSlice";
import floorSlice from "./slices/floorSlice";
import trackerSlice from "./slices/trackerSlice";
import messageSlice from "./slices/messageSlice";

const store = configureStore({
  reducer: {
    user: userSlice.reducer,
    buildings: buildingSlice.reducer,
    floors: floorSlice.reducer,
    trackers: trackerSlice.reducer,
    messages: messageSlice.reducer,
  },
});

export default store;
