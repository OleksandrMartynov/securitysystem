import { Pressable, View, Text, StyleSheet } from "react-native";
import {useState} from "react";

const Button = ({ onPress, title }) => {

  const onPressIn = () => {
    setPressed((state) => {
      return !state;
    })
  }
  const onPressOut = () => {
    setPressed((state) => {
      return !state;
    })
  }
  const [pressed, setPressed] = useState(false)
  return (
    <Pressable style={[styles.button, pressed && styles.buttonOnPressIn]} onPress={onPress} onPressOut={onPressOut} onPressIn={onPressIn}>
      <Text style={styles["button-text"]}>{title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    padding: 15,
    paddingHorizontal: 35,
    backgroundColor: "#015CB1",
    alignItems: "center",
    justifyContent: "center",
  },
  "button-text": {
    fontSize: 16,
    color: "white",
  },
  buttonOnPressIn: {
    opacity: 0.8,
  }
});

export default Button;
