import { useEffect, useRef, useState } from "react";
import { PanResponder, Animated, View } from "react-native";
import { useEvent } from "react-native-reanimated";

const touchableHeight = 20;

const DraggableBox = ({ dragRight, dragLeft, component, onPress }) => {
  const animatedValue = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;

  const [containerHeightValue, setContainerHeightValue] = useState(0);
  const containerHeight = useRef(new Number(0));

  useEffect(() => {
    setContainerHeightValue(containerHeight.current);
  }, [containerHeight.current]);

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: (_, gestureState) => {
        Animated.event([{}, { dx: animatedValue.x, dy: animatedValue.y }], {
          useNativeDriver: false,
        })(_, gestureState);
      },
      onPanResponderRelease: (_, gestureState) => {
        Animated.spring(animatedValue, {
          toValue: { x: 0, y: 0 },
          useNativeDriver: false,
        }).start();
        console.log(containerHeight.current);
        console.log(animatedValue.y._value);
        console.log(animatedValue.x._value);
        if (
          animatedValue.x._value > 100 &&
          Math.abs(animatedValue.y._value) < containerHeight.current / 2
        ) {
          if (dragRight) dragRight();
        } else if (
          animatedValue.x._value < -100 &&
          Math.abs(animatedValue.y._value) < containerHeight.current / 2
        ) {
          if (dragLeft) dragLeft();
        } else if (
          Math.abs(animatedValue.x._value) < 10 &&
          Math.abs(animatedValue.y._value) < containerHeight.current / 2
        ) {
          if (onPress) {
            onPress();
          }
        }
      },
    })
  ).current;

  const handleLayout = (event) => {
    const height = event.nativeEvent.layout.height;
    console.log(height);
    containerHeight.current = height;
    console.log("handleLayout");
  };

  return (
    <Animated.View
      // {...panResponder.panHandlers}
      style={[
        {
          transform: [{ translateX: animatedValue.x }],
          position: "relative",
        },
      ]}
      onLayout={handleLayout}
    >
      <View
        {...panResponder.panHandlers}
        style={{
          zIndex: 1,
          position: "absolute",
          width: "100%",
          top: containerHeightValue / 2 - touchableHeight / 2,
          height: touchableHeight,
        }}
      ></View>
      {component}
    </Animated.View>
  );
};

export default DraggableBox;
