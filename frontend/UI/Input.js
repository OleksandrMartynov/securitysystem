import { TextInput, StyleSheet, View } from "react-native";

const Input = ({ onChange, value }) => {
  return (
    <View style={styles.input}>
      <TextInput onChange={onChange} value={value} />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 10,
    borderColor: "#000000",
    borderWidth: 1,
    paddingVertical: 7,
    paddingHorizontal: 9,
  },
});

export default Input;
